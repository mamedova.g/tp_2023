#include "Dictionary.h";
using Data = std::string;
using Number = std::list<size_t>;
int main()
{
	setlocale(LC_ALL, "Russian");
	Dictionary<Data, Number> dictionary;
	dictionary.printMenu();
	char buffer = '\0';
	std::vector<Command> commands;
	std::istringstream iss("1 C:/Users/alexi/Documents/atest\n1 C:/Users/alexi/Documents/rgr.txt\n4\n2 a567h\n2 la\n5 a\n2 the\n3 the\n2 the\n4\n");
	do
	{
		if (!iss)
		{
			iss.clear();
			iss.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		std::copy
		(
			std::istream_iterator< Command >(iss),
			std::istream_iterator< Command >(),
			std::back_inserter(commands)
		);
	} while (!iss.eof());
	size_t position = 1;
	Data word = "";
	std::ifstream in;
	std::ofstream out("output.txt");
	std::for_each(commands.begin(), commands.end(), [&in, &out, &word, &position, &dictionary](Command& choice)
		{
			std::cout << choice.firstCommand << std::endl;
			switch (choice.firstCommand)
			{
			case 1:
				openFile(in, choice.secondCommand);
				if (!in)
				{
					in.clear();
					in.close();
					break;
				}
				position = 1;
				out << position << ")";
				while (!in.eof())
				{
					if (in.peek() == '\n')
					{
						position += 1;
						out << '\n' << position << ")";
					}
					in >> word;
					out << ' ' << word;
					if (dictionary.checkWord(word))
					{
						dictionary.insertData(word, position);
						std::cout << word << ' ' << dictionary.size() << std::endl;
					}
				}
				out << '\n';
				closeFile(in);
				break;
			case 2:
				std::cout << "������� �����\n";
				std::cout << choice.secondCommand << std::endl;
				if (!dictionary.checkWord(choice.secondCommand))
				{
					std::cout << "������: �� ����� �� �����!\n";
					break;
				}
				if (dictionary.searchData(choice.secondCommand))
				{
					std::cout << "����� " << choice.secondCommand << " ������� � ������� " << std::endl;
				}
				else
				{
					std::cout << "����� " << choice.secondCommand << " ����������� � ������� " << std::endl;
				}
				break;
			case 3:
				std::cout << "������� �����\n";
				std::cout << choice.secondCommand << std::endl;
				if (!dictionary.checkWord(choice.secondCommand))
				{
					std::cout << "������: �� ����� �� �����!\n";
					break;
				}
				dictionary.deleteData(choice.secondCommand);
				break;
			case 4:
				dictionary.makeTable(out);
				break;
			}
		});
	std::cout << "������ ������� " << dictionary.size() << std::endl;
	std::cout << "������������ ������ ������� " << dictionary.maxSize() << std::endl;
	std::cout << "�� ��������� ������ ���������";
	return 0;
}
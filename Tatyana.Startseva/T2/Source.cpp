#include <fstream>
#include <iostream>
#include <sstream>
#include<ostream>
#include <string>
#include <sstream>
#include <vector>
#include<iterator>
#include <cmath>
#include <map>
#include <algorithm>
struct Point
{
	int x, y;
	int get_x()
	{
		return x;
	}
};
struct Polygon
{
	std::vector< Point > points;
	Point getVertex(int index) const {
		return points[index];
	}
	std::vector<Point> getVertices() const {
		return points;
	}
	void addVertex(const Point& vertex) {
		points.push_back(vertex);
	}

};
double area(Polygon& pol, int num_top)
{
	double  p = 0, s = 0;
	for (int i = 0; i < num_top-1; i++)
	{
		p += (pol.points.at(i).x * pol.points.at(i + 1).y - pol.points.at(i + 1).x * pol.points.at(i).y);
	}
	s = 0.5 * std::abs(p);
	return s;
}
double mid_area(std::vector<std::pair<Polygon, int>> pol)
{
	double area_sum = 0;
	
	for (auto it = pol.begin(); it != pol.end(); ++it)
	{
		Polygon temp = it->first;
		int top = it->second;
		area_sum += area(temp, top);
	}
	return area_sum / pol.size();
}
void ever_area(std::vector<std::pair<Polygon, int>> pol)
{
	if (pol.empty()) {
		std::cerr << "The vector of polygons is empty.\n";
		return;
	}
	for (auto i = 0u; i < pol.size(); i++)
	{
		Polygon temp = pol.at(i).first;
		int top = pol.at(i).second;
		if (top % 2 == 0)
		{
			std::cerr << "area shape, which have " << top << " top = " << area(temp, top)<< '\n';
		}
	}
}
void odd_area(const std::vector<std::pair<Polygon, int>>&pol)
{
	if (pol.empty()) {
		std::cerr << "The vector of polygons is empty.\n";
		return;
	}
	for (auto& item : pol)
	{
		Polygon temp = item.first;
		int top = item.second;
		if (top % 2 != 0)
		{
			std::cerr << "Area of a shape with " << top << " vertices = " << area(temp, top) << '\n';
		}
	}
}
void ever_count_top(std::vector<std::pair<Polygon, int>> pol)
{
	int count = 0;
	for (auto i = 0u; i < pol.size(); ++i)
	{
		int top = pol.at(i).second;
		if (top % 2 == 0)
		{
			count++;
		}
	}
	std::cerr << "Count top, which ever = " << count << '\n';

}

void odd_count_top(std::vector<std::pair<Polygon, int>> pol)
{
	int count = 0;
	for (auto i = 0u; i < pol.size(); ++i)
	{
		int top = pol.at(i).second;
		if (top % 2 != 0)
		{
			count++;
		}
	}
	std::cerr << "Count top, which odd = " << count << '\n';
}

std::vector<double> list_area(std::vector<std::pair<Polygon, int>> pol)
{
	std::vector<double> list_area(pol.size());
	for (auto i = 0u; i < pol.size(); ++i)
	{
		Polygon temp = pol.at(i).first;
		int top = pol.at(i).second;
		list_area.at(i) = area(temp, top);

	}
	return list_area;
}
void min_area(std::vector<std::pair<Polygon, int>> pol)
{
	std::vector<double> list_area;
	for (auto i = 0u; i < pol.size(); i++)
	{
		Polygon temp = pol.at(i).first;
		int top = pol.at(i).second;
		list_area.push_back(area(temp, top));

	}
	auto it = std::min_element(list_area.begin(), list_area.end());
	std::cerr << "min area = " << *it << '\n';
}
void max_area(std::vector<std::pair<Polygon, int>> pol)
{
	std::vector<double> list_area;
	for (auto i = 0u; i < pol.size(); i++)
	{
		Polygon temp = pol.at(i).first;
		int top = pol.at(i).second;
		list_area.push_back(area(temp, top));

	}
	std::vector<double>::iterator it = std::max_element(list_area.begin(), list_area.end());
	std::cerr << "Max area = " << *it << '\n';
}
void min_top(std::vector < std::pair<Polygon, int>> pol)
{
	std::vector<int> top;
	for (auto i = 0u; i < pol.size(); i++)
	{
		top.push_back(pol.at(i).second);
	}
	std::vector<int>::iterator it = std::min_element(top.begin(), top.end());
	std::cerr << "Min count top = " << *it << '\n';
}
void max_top(std::vector < std::pair<Polygon, int>> pol)
{
	std::vector<int> top;
	for (auto i = 0u; i < pol.size(); i++)
	{
		top.push_back(pol.at(i).second);
	}
	std::vector<int>::iterator it = std::max_element(top.begin(), top.end());
	std::cerr << "Max count top = " << *it << '\n';
}
void area_num_top(std::vector < std::pair<Polygon, int>> pol, int &num)
{
	for (auto i = 0u; i < pol.size(); i++)
	{
		Polygon temp = pol.at(i).first;
		int top = pol.at(i).second;
		if (top == num)
		{
			std::cerr << " area shape, which have " << num << " top " << area(temp, top) << '\n';
		}
	}
}
void area_num_count(std::vector < std::pair<Polygon, int>> pol, int& num)
{
	int count = 0;
	for (auto i = 0u; i < pol.size(); i++)
	{
		int top = pol.at(i).second;
		if (top == num)
		{
			count++;
		}
	}
	std::cerr << " count shape, which have " << num << " top " << count << '\n';
}

class Rectangle {
public:
	Rectangle(const Point& topLeft, const Point& bottomRight)
		: topLeft_(topLeft), bottomRight_(bottomRight), left_(topLeft.x), top_(topLeft.y), right_(bottomRight.x), bottom_(bottomRight.y) {}

	
	double left() const { return left_; }
	double top() const { return top_; }
	double right() const { return right_; }
	double bottom() const { return bottom_; }

	
	double width() const { return right_ - left_; }
	double height() const { return bottom_ - top_; }

	void setLeft(double left) { left_ = left; }
	void setTop(double top) { top_ = top; }
	void setRight(double right) { right_ = right; }
	void setBottom(double bottom ){bottom_ = bottom; }

private:
	Point topLeft_;
	Point bottomRight_;
	double left_;
	double top_;
	double right_;
	double bottom_;
	
};

bool isInFrame(const Polygon& polygon, const std::vector<std::pair<Polygon, int>>& polygonList) {
	if (polygonList.empty()) {
		return false;
	}

	Point topLeft = polygonList.front().first.getVertex(0);
	Point bottomRight = topLeft;

	for (const auto& pair : polygonList) {
		for (const auto& vertex : pair.first.getVertices()) {
			if (vertex.x < topLeft.x) {
				topLeft.x = vertex.x;
			}
			if (vertex.y < topLeft.y) {
				topLeft.y = vertex.y;
			}
			if (vertex.x > bottomRight.x) {
				bottomRight.x = vertex.x;
			}
			if (vertex.y > bottomRight.y) {
				bottomRight.y = vertex.y;
			}
		}
	}

	Rectangle frame(topLeft, bottomRight);

	for (const auto& vertex : polygon.getVertices()) {
		if (vertex.x < frame.left() || vertex.x > frame.right() || vertex.y < frame.top() || vertex.y > frame.bottom()) {
			return false;
		}
	}

	return true;
}

int main()
{
	std::vector<std::pair<Polygon, int>> polygon;
	std::string command;
	std::ifstream fs;
	std::map<std::string, int> map = {
		{"AREA ODD", 0},
		{"AREA EVEN", 1},
		{"AREA MEAN", 2},
		{"MAX AREA", 3},
		{"MAX VERTEXES", 4},
		{"MIN AREA", 5},
		{"MIN VERTEXES", 6},
		{"COUNT EVEN", 7},
		{"COUNT ODD", 8}
	};
	/*std::string way = "filename.txt";*/
	try
	{
		/*fs.open(way);
		if (!fs)
		{
			throw std::exception("invalid way ");
		}*/
		std::string data = "4 (0;0) (1;0) (1;1) (0;1)\n 4 (1; 1) (2; 1) (2; 2) (1; 2)\n INFRAME 3 (0; 0) (2; 2) (2; 0)";
		std::istringstream iss(data);
		std::string line;

		while (std::getline(iss, line))
		{
			std::istringstream sstream(line);
			int num_top;
			sstream >> num_top;
			if (num_top == 0)
			{
				command = line;
			}
			else {
				Polygon shape;
				for (int i = 0; i < num_top; i++)
				{
					char bracket;
					int x, y;
					sstream >> bracket >> x >> bracket >> y >> bracket;
					shape.points.push_back({ x, y });
				}
				polygon.push_back(std::make_pair(shape, num_top));
			}
		}
		std::istringstream read(command);
		std::string str;
		read >> str;
		if (str == "INFRAME") {
			Polygon figure;
			int num_top;
			int x, y;
			read >> num_top;
			for (int i = 0; i < num_top; i++) {
				char bracket;
				read >> bracket >> x >> bracket >> y >> bracket;
				figure.points.push_back({ x, y });
			}
			bool isInsideFrame = isInFrame(figure, polygon);
			if (isInsideFrame) {
				std::cout << "<TRUE>\n";
			}
			else {
				std::cout << "<FALSE>\n";
			}
		}
		else if (str == "AREA")
		{
			
			int num = 0;
			read >>  num;
			area_num_top(polygon, num);
		}
		else if (str == "COUNT")
		{
			
			int num = 0;
			read >>  num;
			area_num_count(polygon, num);
		}
		else
		{
			int number_command = map[command];
			switch (number_command)
			{
			case (0):
			{
				odd_area(polygon);
				break;
			}
			case (1):
			{
				ever_area(polygon);
				break;
			}
			case (2):
			{
				std::cerr << "AREA MEAN   " << mid_area(polygon);
				break;
			}
			case (3):
			{
				max_area(polygon);
				break;
			}
			case(4):
			{
				max_top(polygon);
				break;
			}
			case(5):
			{
				min_area(polygon);
				break;
			}
			case(6):
			{
				min_top(polygon);
				break;
			}
			case(7):
			{
				ever_count_top(polygon);
				break;
			}
			case(8):
			{
				odd_count_top(polygon);
				break;
			}
			default:
				break;
			}
		}
	}
	catch (const std::exception &ex)
	{
		std::cerr << ex.what() << '\n';
	}
	
	return 0;
}
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <bitset>
#include <iomanip>
#include <sstream>
#include <iterator>
struct DataStruct {
    unsigned long long key1;
    char key2;
    std::string key3;
};

std::istream& operator>>(std::istream& is, DataStruct& d) {
    char c1, c2, c3, c4;
    is >> c1 >> c2 >> d.key1 >> c2 >> c3 >> d.key2 >> c3 >> c4;
    std::getline(is >> std::ws, d.key3, c4);
    return is;
}

std::ostream& operator<<(std::ostream& os, const DataStruct& d) {
    os << "(key1 " << d.key1 << "ull:key2 '" << d.key2 << "':key3 \"" << d.key3 << "\":)";
    return os;
}

bool operator<(const DataStruct& d1, const DataStruct& d2) {
    if (d1.key1 < d2.key1) {
        return true;
    }
    else if (d1.key1 == d2.key1) {
        if (d1.key2 < d2.key2) {
            return true;
        }
        else if (d1.key2 == d2.key2) {
            return d1.key3.length() < d2.key3.length();
        }
    }
    return false;
}

std::string double_to_scientific_string(double value) {
    std::ostringstream ss;
    ss << std::scientific << std::setprecision(2) << value;
    std::string str = ss.str();
    if (str[0] == '-') {
        str.erase(0, 1);
    }
    if (str[0] == '0') {
        str.erase(0, 1);
    }
    return str;
}

std::string ull_to_binary_string(unsigned long long value) {
    std::ostringstream ss;
    ss << "0b" << std::bitset<sizeof(unsigned long long) * 8>(value).to_string();
    return ss.str();
}
int main() {
    
    std::string input_string = "(:key1 10ull:key2 'c':key3 \"Data\":)\n(:key3 \"Data\":key2 'c':key1 10ull:)";
    std::istringstream iss(input_string);

   std::vector<DataStruct> data;
    DataStruct temp;
    std::istream_iterator<DataStruct> start(iss), end;
    std::copy(start, end, std::back_inserter(data));
    std::sort(data.begin(), data.end());
    std::ostream_iterator<DataStruct> out_it(std::cout, "\n");
    std::copy(data.begin(), data.end(), out_it);

    return 0;
}
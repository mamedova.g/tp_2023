﻿#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>
#include <limits>

struct Point
{
    int x, y;
};

struct Polygon
{
    std::vector<Point> points;
};

bool operator==(const Point& lhs, const Point& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

std::istream& operator>>(std::istream& is, Point& p)
{
    char c1, c2, c3;
    return is >> c1 >> p.x >> c2 >> p.y >> c3;
}

Polygon parse_polygon(const std::string& line)
{
    Polygon poly;
    std::istringstream iss(line);
    int n;
    iss >> n;
    for (int i = 0; i < n; ++i)
    {
        Point p;
        iss >> p;
        poly.points.push_back(p);
        if (i < n - 1)
        {
            char separator;
            iss >> separator;
        }
    }
    return poly;
}

double count_area(const Polygon& poly)
{
    double result = 0.0;
    for (size_t i = 0; i < poly.points.size(); ++i)
    {
        size_t j = (i + 1) % poly.points.size();
        result += (poly.points[i].x * poly.points[j].y) - (poly.points[j].x * poly.points[i].y);
    }
    return std::abs(result) / 2.0;
}

int count_polygons(const std::vector<Polygon>& polygons, const std::string& parameter)
{
    int count = 0;
    if (parameter == "EVEN" || parameter == "ODD")
    {
        bool is_even = parameter == "EVEN";
        for (const auto& poly : polygons)
        {
            if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(is_even))
            {
                count++;
            }
        }
    }
    else
    {
        try
        {
            int num_of_vertexes = std::stoi(parameter);
            for (const auto& poly : polygons)
            {
                if (static_cast<int>(poly.points.size()) == num_of_vertexes)
                {
                    count++;
                }
            }
        }
        catch (const std::invalid_argument& e)
        {
            std::cerr << "Error: wrong parameter for COUNT command." << std::endl;
        }
    }
    return count;
}

bool check_permutation(const Polygon& poly1, const Polygon& poly2)
{
    if (poly1.points.size() != poly2.points.size()) {
        return false;
    }

    std::vector<bool> matched(poly2.points.size(), false);
    for (const auto& p1 : poly1.points) {
        bool found = false;
        for (size_t j = 0; j < poly2.points.size(); ++j) {
            if (!matched[j] && p1 == poly2.points[j]) {
                matched[j] = true;
                found = true;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }

    return true;
}

int count_perms(const std::vector<Polygon>& polygons, const Polygon& target_poly)
{
    int count = 0;
    for (const auto& poly : polygons)
    {
        if (check_permutation(poly, target_poly))
        {
            count++;
        }
    }
    return count;
}

bool is_right_angle(const Point& p1, const Point& p2, const Point& p3)
{
    int dx1 = p2.x - p1.x;
    int dy1 = p2.y - p1.y;
    int dx2 = p3.x - p2.x;
    int dy2 = p3.y - p2.y;

    return dx1 * dx2 + dy1 * dy2 == 0;
}

bool is_right_shape(const Polygon& poly)
{
    size_t n = poly.points.size();
    if (n < 3) {
        return false;
    }

    for (size_t i = 0; i < n; ++i) {
        size_t j = (i + 1) % n;
        size_t k = (i + 2) % n;
        if (!is_right_angle(poly.points[i], poly.points[j], poly.points[k])) {
            return false;
        }
    }

    return true;
}

int count_right_shapes(const std::vector<Polygon>& polygons)
{
    int count = 0;
    for (const auto& poly : polygons) {
        if (is_right_shape(poly)) {
            count++;
        }
    }
    return count;
}

int main()
{
    std::string input_filename = "shapes.txt";
    std::string commands_filename = "commands.txt";

    std::ifstream input(input_filename);
    if (!input)
    {
        std::cerr << "Error: unable to open input file." << std::endl;
        return 1;
    }

    std::vector<Polygon> polygons;
    std::string line;
    while (std::getline(input, line))
    {
        if (!line.empty())
        {
            polygons.push_back(parse_polygon(line));
        }
    }
    input.close();

    std::ifstream commands_file(commands_filename);
    if (!commands_file)
    {
        std::cerr << "Error: unable to open file with commands." << std::endl;
        return 1;
    }

    while (std::getline(commands_file, line))
    {
        std::istringstream commands(line);
        std::string command;
        commands >> command;

        if (command == "AREA")
        {
            std::string parameter;
            commands >> parameter;
            double total_area = 0.0;
            if (parameter == "EVEN" || parameter == "ODD")
            {
                bool is_even = parameter == "EVEN";
                for (const auto& poly : polygons)
                {
                    if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(is_even))
                    {
                        total_area += count_area(poly);
                    }
                }
                std::cout << "AREA " << parameter << ": ";
            }
            else if (parameter == "MEAN")
            {
                if (polygons.empty())
                {
                    std::cout << "No polygons to calculate the mean area." << std::endl;
                    continue;
                }
                else
                {
                    for (const auto& poly : polygons)
                    {
                        total_area += count_area(poly);
                    }
                    total_area /= polygons.size();
                }
                std::cout << "AREA MEAN: ";
            }
            else
            {
                try
                {
                    int num_of_vertexes = std::stoi(parameter);
                    for (const auto& poly : polygons)
                    {
                        if (static_cast<int>(poly.points.size()) == num_of_vertexes)
                        {
                            total_area += count_area(poly);
                        }
                    }
                    std::cout << "AREA " << num_of_vertexes << ": ";
                }
                catch (const std::invalid_argument& e)
                {
                    std::cerr << "Error: wrong parameter for AREA command." << std::endl;
                    continue;
                }
            }
            std::cout << std::fixed << std::setprecision(1) << total_area << std::endl;
        }
        else if (command == "MAX" || command == "MIN")
        {
            std::string area_command;
            commands >> area_command;
            if (area_command != "AREA")
            {
                std::cerr << "Error: wrong command." << std::endl;
                continue;
            }
            if (polygons.empty())
            {
                std::cout << "No polygons to calculate the " << command << "area." << std::endl;
                continue;
            }
            double result_area = count_area(polygons[0]);
            for (const auto& poly : polygons)
            {
                double current_area = count_area(poly);
                if (command == "MAX")
                {
                    if (current_area > result_area)
                    {
                        result_area = current_area;
                    }
                }
                else
                {
                    if (current_area < result_area)
                    {
                        result_area = current_area;
                    }
                }
            }
            std::cout << command << " AREA: " << std::fixed << std::setprecision(1) << result_area << std::endl;
        }
        else if (command == "COUNT")
        {
            std::string parameter;
            commands >> parameter;
            int count = count_polygons(polygons, parameter);
            std::cout << "COUNT " << parameter << ": " << count << std::endl;
        }
        else if (command == "PERMS")
        {
            std::string poly_str;
            std::getline(commands, poly_str);
            Polygon target_poly = parse_polygon(poly_str);
            int num_perms = count_perms(polygons, target_poly);
            std::cout << "PERMS " << num_perms << std::endl;
        }
        else if (command == "RIGHTSHAPES")
        {
            std::string poly_str;
            std::getline(commands, poly_str);
            Polygon target_poly = parse_polygon(poly_str);
            int num_right_shapes = count_right_shapes(polygons);
            std::cout << "RIGHTSHAPES " << num_right_shapes << std::endl;
        }
        else
        {
            std::cerr << "Error: invalid command." << std::endl;
        }
    }

    commands_file.close();

    return 0;
}
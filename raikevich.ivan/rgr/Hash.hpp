#ifndef HASH_DICTIONARY
#define HASH_DICTIONARY

#include <string>
#include <iostream>
#include <algorithm>
#include <cctype>
#include <unordered_map>

const std::size_t TABLE_SIZE = 430;

class FrequencyDictionary {
private:
    struct Node {
        std::string word;
        int frequency;
        Node* next;
    };

    std::unordered_map<std::string, Node*> table;

public:
    FrequencyDictionary() {}

    std::size_t hashFunction(const std::string& word) {
        std::size_t sum = 0;
        for (char c : word) {
            if (c != ',' && c != '.')
                sum += static_cast<int>(std::tolower(c));
        }
        return sum % TABLE_SIZE;
    }

    void insert(const std::string& word) {
        std::string cleanWord;
        for (char c : word) {
            if (c != ',' && c != '.')
                cleanWord += std::tolower(c);
        }


        if (table.find(cleanWord) != table.end()) {
            table[cleanWord]->frequency++;
        }
        else {
            Node* newNode = new Node;
            newNode->word = cleanWord;
            newNode->frequency = 1;
            newNode->next = nullptr;
            table[cleanWord] = newNode;
        }
    }

    std::string getThreeMostFrequentWords() {
        std::string result;
        int maxFrequency1 = 0, maxFrequency2 = 0, maxFrequency3 = 0;
        std::string word1, word2, word3;

        for (const auto& pair : table) {
            Node* currNode = pair.second;
            while (currNode != nullptr) {
                if (currNode->frequency > maxFrequency1) {
                    maxFrequency3 = maxFrequency2;
                    word3 = word2;
                    maxFrequency2 = maxFrequency1;
                    word2 = word1;
                    maxFrequency1 = currNode->frequency;
                    word1 = currNode->word;
                }
                else if (currNode->frequency > maxFrequency2) {
                    maxFrequency3 = maxFrequency2;
                    word3 = word2;
                    maxFrequency2 = currNode->frequency;
                    word2 = currNode->word;
                }
                else if (currNode->frequency > maxFrequency3) {
                    maxFrequency3 = currNode->frequency;
                    word3 = currNode->word;
                }

                currNode = currNode->next;
            }
        }

        result += search(word1) + "\n";
        result += search(word2) + "\n";
        result += search(word3) + "\n";

        return result;
    }

    std::string search(const std::string& word) {
        std::string cleanWord;
        for (char c : word) {
            if (c != ',' && c != '.')
                cleanWord += std::tolower(c);
        }

        if (table.find(cleanWord) != table.end()) {
            Node* currNode = table[cleanWord];
            return "Word: " + currNode->word + " (" + std::to_string(currNode->frequency) + " times)";
        }
        else {
            return "Word not found in the dictionary.";
        }
    }

    bool remove(const std::string& word) {
        std::string cleanWord;
        for (char c : word) {
            if (c != ',' && c != '.')
                cleanWord += std::tolower(c);
        }

        if (table.find(cleanWord) != table.end()) {
            Node* currNode = table[cleanWord];
            table.erase(cleanWord);
            delete currNode;
            return true;
        }

        return false;
    }

    void printHashTable(const FrequencyDictionary& dictionary) {
        for (const auto& pair : dictionary.table) {
            Node* currNode = pair.second;
            while (currNode != nullptr) {
                std::cout << "Word: " << currNode->word << ", Frequency: " << currNode->frequency << std::endl;
                currNode = currNode->next;
            }
        }
    }

    ~FrequencyDictionary() {
        for (const auto& pair : table) {
            Node* currNode = pair.second;
            while (currNode != nullptr) {
                Node* nextNode = currNode->next;
                delete currNode;
                currNode = nextNode;
            }
        }
    }
};

#endif // HASH_DICTIONARY

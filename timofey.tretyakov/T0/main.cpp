#include <iostream>
using namespace std;
class A {
public:
	A(int p) :
	p_(p) {cout << "A(p)_";}
	A(const A& other) : p_(other.p_) { cout << "A(const A&)_"; }
	int getP() const { return  p_; };
	void setP(int p) { p_ = p; }
private:
	int p_;
};

bool isEqual(A x, A y) {
	return x.getP() == y.getP();
}

int main() {
	A ob1(3);
	A ob2(5);
	A ob3(ob1);
	bool result = isEqual(ob1,ob2);
}


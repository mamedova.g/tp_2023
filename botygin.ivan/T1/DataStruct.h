#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>

#pragma once
class DataStruct
{
};

struct Data
{
    double key1;
    unsigned long long key2;
    std::string key3;
};

struct DelimiterIO
{
    char exp;
};

struct KeyIO
{
    std::string& name;
};

struct DoubleIO
{
    double& ref;
};

struct HexIO
{
    unsigned long long& ref;
};

struct StringIO
{
    std::string& ref;
};

bool operator<(const Data& data1, const Data& data2);
std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
std::istream& operator>>(std::istream& in, DoubleIO&& dest);
std::istream& operator>>(std::istream& in, HexIO&& dest);
std::istream& operator>>(std::istream& in, StringIO&& dest);
std::istream& operator>>(std::istream& in, Data& dest);
std::ostream& operator<<(std::ostream& out, const Data& dest);

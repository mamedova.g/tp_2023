#include <iostream>
#include <sstream>
#include "DataStruct.h"

int main() {
    
    std::cout << "Test: Read data from std::istringstream\n";
    std::cout << "Data vector:\n";

    std::istringstream iss("(:key1 8ull:key2 0o113:key3 \"Data\":)\n"
        "(:key1 3ull:key2 0o09:key3 \"Hello\":)\n"
        "(:key1 7ull:key2 0o076:key3 \"World\":)\n"
        "(:key1 10ull:key2 0o11:key3 \"DataStruct\":)\n"
        "(:key1 4ull:key2 0o07:key3 \"Hello\":)\n"
        "(:key1 6ull:key3 \"World\":)\n"
        "(:key1 5ull:key2 0o07:key3 \"Invalid\":\n");

    DataStruct data;
    std::vector<DataStruct> dataVector;
    std::copy(std::istream_iterator<DataStruct>(iss), std::istream_iterator<DataStruct>(), std::back_inserter(dataVector));
    std::sort(dataVector.begin(), dataVector.end());
    std::copy(dataVector.begin(), dataVector.end(), std::ostream_iterator<DataStruct>(std::cout, "\n"));

  
    
    std::cout << '\n';

    return 0;

}


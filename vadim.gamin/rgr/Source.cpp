#include <iostream>
#include <string>
#include <map>
#include <locale>
//#include "windows.h"
#include <algorithm>


class AVLDictionary {
private:
    std::map<std::string, std::multimap<std::string, std::string>> dictionary;

public:
    void insert(const std::string& key, const std::string& translation) {
        for (const unsigned char c : key) {
            if (!isalpha(c)) {
                throw std::invalid_argument("������������ ������ �����. ��������� ������ ���������� �����.");
            }
        }
        //for (const unsigned char c : translation) {
        //    if (!isRussian(c)) {
        //        throw std::invalid_argument("������������ ������ ��������. ��������� ������ ������� �����.");
        //    }
        //}
        dictionary[toLowercase(key)].insert(std::make_pair(key, translation));
    }

    std::multimap<std::string, std::string> search(const std::string& key) {
        auto it = dictionary.find(toLowercase(key));
        if (it != dictionary.end()) {
            return it->second;
        }
        else {
            throw std::invalid_argument("����� �� �������");
        }
    }

    void remove(const std::string& key) {
        auto it = dictionary.find(toLowercase(key));
        if (it != dictionary.end()) {
            dictionary.erase(it);
        }
        else {
            throw std::invalid_argument("����� �� �������");
        }
    }

    void printDictionary() {
        if (dictionary.empty()) {
            std::cout << "������� ����." << std::endl;
            return;
        }

        for (const auto& entry : dictionary) {
            std::cout << entry.first << ":\n";
            for (const auto& translation : entry.second) {
                std::cout << "  " << translation.second << std::endl;
            }
        }
    }

private:
    std::string toLowercase(const std::string& str) {
        std::string result = str;
        std::transform(result.begin(), result.end(), result.begin(), ::tolower);
        return result;
    }

  /*  bool isRussian(unsigned char c) {
        return (c >= 192 && c <= 255);
    }*/
};

int main() {
    try {
        //SetConsoleOutputCP(1251);
        //SetConsoleCP(1251);
        AVLDictionary dictionary;

        std::cout << "����� ���������� � �������!" << std::endl;

        while (true) {
            std::cout << "\n�������� ��������:\n";
            std::cout << "1. �������� ����� � �������.\n";
            std::cout << "2. ����� ������� ����������� �����.\n";
            std::cout << "3. ������� �����.\n";
            std::cout << "4. �������� ���� �������.\n";
            std::cout << "5. ����� �� ���������.\n";
            std::cout << "��� �����: ";

            std::string choice;
            std::cin >> choice;
            try {
                if (std::cin.fail() || choice < "1" || choice > "5") {
                    std::cin.clear();
                    throw std::invalid_argument("������������ �����. ���������� �����.");
                }
            }
            catch (const std::invalid_argument& e) {
                std::cerr << "������ " << e.what() << std::endl;
            }
           

            if (choice == "1") {
                std::string word, translation;
                std::cout << "\n������� ����� �� ����������: ";
                std::cin >> word;
                std::cout << "������� ������� ����� �� �������: ";
                std::cin.ignore(); // ������� ������ ����� �������������� std::getline()
                std::getline(std::cin, translation);

                try {
                    dictionary.insert(word, translation);
                    std::cout << "����� ������� ��������� � �������." << std::endl;
                }
                catch (const std::invalid_argument& e) {
                    std::cerr << "������ " << e.what() << std::endl;
                }
                
            }
            else if (choice == "2") {
                std::string word;
                std::cout << "\n������� ���������� ����� ��� ������ ��������: ";
                std::cin >> word;

                try {
                    std::multimap<std::string, std::string> translations = dictionary.search(word);
                    std::cout << "������� ����� " << word << ":\n";
                    for (const auto& translation : translations) {
                        std::cout << "  " << translation.second << std::endl;
                    }
                }
                catch (const std::invalid_argument& e) {
                    std::cerr << "������: " << e.what() << std::endl;
                }
                
   

            }
            else if (choice == "3") {
                std::string word;
                std::cout << "\n������� ����� ��� ��������: ";
                std::cin >> word;
                try {
                    dictionary.remove(word);
                    std::cout << "����� ������� ������� �� �������." << std::endl;
                }
                catch (const std::invalid_argument& e) {
                    std::cerr << "������: " << e.what() << std::endl;
                }
            }
            else if (choice == "4") {
                std::cout << "\n�������:\n";
                dictionary.printDictionary();
            }
            else if (choice == "5") {
                std::cout << "\n�� ��������!" << std::endl;
                break;
            }
        }
    }
    catch (const std::exception& e) {
        std::cerr << "������: " << e.what() << std::endl;
        return -1;
    }

    return 0;
}
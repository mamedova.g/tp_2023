#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <iomanip>
#include <limits>
#include <algorithm>
#include <functional>

struct Point
{
	int x, y;
};

struct Polygon
{
	std::vector<Point> points;
};

bool operator==(const Point& lhs, const Point& rhs)
{
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

std::istream& operator>>(std::istream& is, Point& p)
{
	char c1, c2, c3;
	return is >> c1 >> p.x >> c2 >> p.y >> c3;
}

Polygon parsePolygon(const std::string& line)
{
	Polygon poly;
	std::istringstream iss(line);
	int n;
	iss >> n;
	for (int i = 0; i < n; ++i)
	{
		Point p;
		iss >> p;
		poly.points.push_back(p);
		if (i < n - 1)
		{
			char separator;
			iss >> separator;
		}
	}
	return poly;
}

double countArea(const Polygon& poly)
{
	double result = 0.0;
	for (size_t i = 0; i < poly.points.size(); ++i)
	{
		size_t j = (i + 1) % poly.points.size();
		result += (poly.points[i].x * poly.points[j].y) - (poly.points[j].x * poly.points[i].y);
	}
	return std::abs(result) / 2.0;
}

bool isSizeEvenOdd(const size_t size, const bool isEven)
{
	return size % 2 == static_cast<int>(isEven);
}

bool isSizeEqualToNum(const size_t size, const int num)
{
	return static_cast<int>(size) == num;
}


int countPolygons(const std::vector<Polygon>& polygons, const std::string& parameter)
{
	int count = 0;
	if (parameter == "EVEN" || parameter == "ODD")
	{
		bool isEven = parameter == "EVEN";
		auto get_points_size = [](const Polygon& p) { return p.points.size(); };
		count = std::count_if(polygons.begin(), polygons.end(),
			std::bind(isSizeEvenOdd, std::bind(get_points_size, std::placeholders::_1), isEven));
	}
	else
	{
		try
		{
			int numVertexes = std::stoi(parameter);
			auto get_points_size = [](const Polygon& p) { return p.points.size(); };
			count = std::count_if(polygons.begin(), polygons.end(),
				std::bind(isSizeEqualToNum, std::bind(get_points_size, std::placeholders::_1), numVertexes));
		}
		catch (const std::invalid_argument& e)
		{
			std::cerr << "Error: wrong parameter for COUNT command." << std::endl;
		}
	}
	return count;
}

bool checkPermutation(const Polygon& poly1, const Polygon& poly2)
{
	if (poly1.points.size() != poly2.points.size())
	{
		return false;
	}

	std::vector<bool> matched(poly2.points.size(), false);
	for (const auto& p1 : poly1.points)
	{
		bool found = false;
		for (size_t j = 0; j < poly2.points.size(); ++j)
		{
			if (!matched[j] && p1 == poly2.points[j])
			{
				matched[j] = true;
				found = true;
				break;
			}
		}
		if (!found)
		{
			return false;
		}
	}

	return true;
}

bool isRightAngle(const Point& p1, const Point& p2, const Point& p3)
{
	int dx1 = p2.x - p1.x;
	int dy1 = p2.y - p1.y;
	int dx2 = p3.x - p2.x;
	int dy2 = p3.y - p2.y;

	return dx1 * dx2 + dy1 * dy2 == 0;
}

bool isRightShape(const Polygon& poly)
{
	size_t n = poly.points.size();
	if (n < 3)
	{
		return false;
	}

	for (size_t i = 0; i < n; ++i)
	{
		size_t j = (i + 1) % n;
		size_t k = (i + 2) % n;
		if (!isRightAngle(poly.points[i], poly.points[j], poly.points[k]))
		{
			return false;
		}
	}

	return true;
}

int countRightShapes(const std::vector<Polygon>& polygons)
{
	return std::count_if(polygons.begin(), polygons.end(), isRightShape);
}

int echoPolygon(std::vector<Polygon>& polygons, const Polygon& polygon)
{
	int count = 0;
	for (auto it = polygons.begin(); it != polygons.end(); ++it)
	{
		if (checkPermutation(*it, polygon))
		{
			it = polygons.insert(++it, polygon);
			count++;
		}
	}
	return count;
}



int main()
{
	std::string inputFilename = "shapes.txt";
	std::string commandsFilename = "commands.txt";

	std::ifstream input(inputFilename);
	if (!input)
	{
		std::cout << "File " << inputFilename << " not opened" << std::endl;
		return 0;
	}

	std::ifstream commandsFile(commandsFilename);
	if (!commandsFile)
	{
		std::cout << "File " << commandsFilename << " not opened" << std::endl;
		return 0;
	}

	std::vector<Polygon> polygons;
	std::string line;
	while (std::getline(input, line))
	{
		if (!line.empty())
		{
			polygons.push_back(parsePolygon(line));
		}
	}
	input.close();

	while (std::getline(commandsFile, line))
	{
		std::istringstream commands(line);
		std::string command;
		commands >> command;

		if (command == "AREA")
		{
			std::string parameter;
			commands >> parameter;
			double totalArea = 0.0;
			if (parameter == "EVEN" || parameter == "ODD")
			{
				bool isEven = parameter == "EVEN";
				for (const auto& poly : polygons)
				{
					if (static_cast<int>(poly.points.size()) % 2 == static_cast<int>(isEven))
					{
						totalArea += countArea(poly);
					}
				}
				std::cout << "AREA " << parameter << ": ";
			}
			else if (parameter == "MEAN")
			{
				if (polygons.empty())
				{
					std::cout << "No polygons to calculate the mean area." << std::endl;
					continue;
				}
				else
				{
					for (const auto& poly : polygons)
					{
						totalArea += countArea(poly);
					}
					totalArea /= polygons.size();
				}
				std::cout << "AREA MEAN: ";
			}
			else
			{
				try
				{
					int numVertexes = std::stoi(parameter);
					for (const auto& poly : polygons)
					{
						if (static_cast<int>(poly.points.size()) == numVertexes)
						{
							totalArea += countArea(poly);
						}
					}
					std::cout << "AREA " << numVertexes << ": ";
				}
				catch (const std::invalid_argument& e)
				{
					std::cerr << "Error: wrong parameter for AREA command." << std::endl;
					continue;
				}
			}
			std::cout << std::fixed << std::setprecision(1) << totalArea << std::endl;
		}
		else if (command == "MAX" || command == "MIN")
		{
			std::string areaCommand;
			commands >> areaCommand;
			if (areaCommand != "AREA")
			{
				std::cerr << "Error: wrong command." << std::endl;
				continue;
			}
			if (polygons.empty())
			{
				std::cout << "No polygons to calculate the " << command << "area." << std::endl;
				continue;
			}
			double result_area = countArea(polygons[0]);
			for (const auto& poly : polygons)
			{
				double current_area = countArea(poly);
				if (command == "MAX")
				{
					if (current_area > result_area)
					{
						result_area = current_area;
					}
				}
				else
				{
					if (current_area < result_area)
					{
						result_area = current_area;
					}
				}
			}
			std::cout << command << " AREA: " << std::fixed << std::setprecision(1) << result_area << std::endl;
		}
		else if (command == "COUNT")
		{
			std::string parameter;
			commands >> parameter;
			int count = countPolygons(polygons, parameter);
			std::cout << "COUNT " << parameter << ": " << count << std::endl;
		}
		else if (command == "ECHO")
		{
			std::string polygon_str;
			std::getline(commands, polygon_str);
			Polygon polygon = parsePolygon(polygon_str);
			int numDuplicates = echoPolygon(polygons, polygon);
			std::cout << "ECHO" << polygon_str << ": " << numDuplicates << std::endl;
		}
		else if (command == "RIGHTSHAPES")
		{
			std::string polygonString;
			std::getline(commands, polygonString);
			Polygon targetPoly = parsePolygon(polygonString);
			int numRightShapes = countRightShapes(polygons);
			std::cout << "RIGHTSHAPES " << numRightShapes << std::endl;
		}
		else
		{
			std::cerr << "<INVALID COMMAND>" << std::endl;
		}
	}

	commandsFile.close();

	return 0;
}
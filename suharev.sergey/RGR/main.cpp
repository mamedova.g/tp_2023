#include <iostream>
#include <sstream>
#include "Header.hpp"

int main()
{
    AVLTree tree;
    
    std::cout << "Добро пожаловать в англо-русский словарь на основе AVL-дерева.\n";
    std::cout << "Введите команду (add, remove, search или exit):\n";

    std::string command;
    while (std::getline(std::cin, command))
    {
        std::istringstream commandStream(command);
        std::string operation;
        commandStream >> operation;

        if (operation == "add")
        {
            std::string key, value;
            commandStream >> key >> value;
            tree.insert(key, value);
            std::cout << "Добавлено: " << key << " - " << value << '\n';
        }
        else if (operation == "remove")
        {
            std::string key;
            commandStream >> key;
            tree.remove(key);
            std::cout << "Удалено: " << key << '\n';
        }
        else if (operation == "search")
        {
            std::string key;
            commandStream >> key;
            std::list<std::string> translations = tree.search(key);
            if (translations.empty())
            {
                std::cout << "Не найдено" << '\n';
            }
            else
            {
                std::cout << "Переводы для '" << key << "': ";
                for (std::string translation : translations)
                {
                    std::cout << translation << " ";
                }
                std::cout << '\n';
            }
        }
        else if (operation == "exit")
        {
            break;
        }
        else
        {
            std::cout << "Неизвестная команда. Введите add, remove, search или exit." << '\n';
        }

        std::cout << "Введите команду (add, remove, search или exit):\n";
    }

    return 0;
}
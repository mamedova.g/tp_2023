#ifndef GEOMETRY
#define GEOMETRY
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cstdlib>
#include <set>
#include <algorithm>
#include <numeric>
#include <functional>
#include <iomanip>

struct Point
{
	int x, y;
};
struct Polygon
{
	std::vector< Point > points;
};

bool isRightFormat(std::string str)
{
	std::string str1 = str.substr(0, str.find(' '));
	if (std::all_of(str1.begin(), str1.end(), ::isdigit))
	{
		std::string str2 = str.substr(str.find(' ') + 1);
		std::string str3;
		std::string str4;
		std::string x_str;
		std::string y_str;
		int counter = 0;
		while (!str2.empty())
		{
			str3 = str2.substr(0, str2.find(' '));
			if (str3.at(0) == '(' && str3.back() == ')')
			{
				str4 = str3.substr(1, str3.length() - 2);
				size_t semicolon_pos = str4.find(';');
				if (semicolon_pos == std::string::npos)
				{
					return false;
				}
				else
				{
					std::string x_str = str4.substr(0, semicolon_pos);
					std::string y_str = str4.substr(semicolon_pos + 1);
					if (!std::all_of(x_str.begin(), x_str.end(), ::isdigit) || !std::all_of(y_str.begin(), y_str.end(), ::isdigit))
					{
						return false;
					}
				}
			}
			else
			{
				return false;
			}
			auto pos = str2.find(' ');
			if (pos == std::string::npos)
			{
				str2.clear();
			}
			else
			{
				str2.erase(0, pos + 1);
			}
			counter++;
		}
		if (counter == std::stoi(str1))
		{
			return true;
		}
		return false;
	}
	else
	{
		return false;
	}
}

std::istream& operator>>(std::istream& is, Point& point)
{
	std::string str;
	is >> str;
	str = str.substr(1, str.length() - 2);
	size_t semicolon_pos = str.find(';');
	if (semicolon_pos != std::string::npos) {
		std::string x_str = str.substr(0, semicolon_pos);
		std::string y_str = str.substr(semicolon_pos + 1);
		point.x = std::stoi(x_str);
		point.y = std::stoi(y_str);
	}
	return is;
}

std::istream& operator>>(std::istream& is, Polygon& polygon)
{
	std::string str;
	std::streampos prev_pos = is.tellg();
	std::getline(is, str);
	if (str.empty())
	{
		std::getline(is, str);
	}
	is.seekg(prev_pos);

	if (!isRightFormat(str))
	{
		is.setstate(std::ios::failbit);
		return is;
	}
	int num_points;
	is >> num_points;
	while (num_points)
	{
		Point point;
		is >> point;
		polygon.points.push_back(point);
		num_points--;
	}
	return is;
}

double area(const Polygon& polygon)
{
	if (polygon.points.size() < 3)
	{
		return 0.0;
	}
	std::vector<Point>::const_iterator prev = polygon.points.end() - 1;
	std::vector<double> products(polygon.points.size());
	std::transform(polygon.points.begin(), polygon.points.end(), products.begin(),
		std::bind([](const Point& p1, const Point& p2) { return p1.x * p2.y - p2.x * p1.y; }, std::placeholders::_1, *prev));
	double sum = std::accumulate(products.begin(), products.end(), 0.0);
	return std::abs(sum) / 2.0;
}

double AREA(std::string str, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> Shapes;
	if (str == "EVEN")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 == 0; });
	}
	if (str == "ODD")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 != 0; });
	}
	double total_area = std::accumulate(Shapes.begin(), Shapes.end(), 0.0,
		[](double acc, const Polygon& shape) {
			return acc + area(shape);
		});
	return total_area;
}

double AREA(const std::vector<Polygon>& polygons)
{
	if (polygons.size() > 0)
	{
		double sum = std::accumulate(polygons.begin(), polygons.end(), 0.0, [](double acc, const Polygon& p)
			{return acc + area(p); });
		return sum / polygons.size();
	}
	return 0;
}

double AREA(std::size_t size, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> Shapes;
	std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(Shapes), [size](const Polygon& polygon)
		{return polygon.points.size() == size; });
	double total_area = std::accumulate(Shapes.begin(), Shapes.end(), 0.0,
		[](double acc, const Polygon& shape) {
			return acc + area(shape);
		});
	return total_area;
}

double MAX(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "AREA" && polygons.size() > 0)
	{
		std::vector<double> areas(polygons.size());
		std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(area, std::placeholders::_1));
		auto max_area = *std::max_element(areas.begin(), areas.end());
		return max_area;
	}
	if (str == "VERTEXES" && polygons.size() > 0)
	{
		std::vector<int> sizes(polygons.size());
		std::transform(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size();
			});
		auto max_size = *std::max_element(sizes.begin(), sizes.end());
		return max_size;
	}
	return 0;
}

double MIN(std::string str, const std::vector<Polygon>& polygons)
{
	if (str == "AREA" && polygons.size() > 0)
	{
		std::vector<double> areas(polygons.size());
		std::transform(polygons.begin(), polygons.end(), areas.begin(), std::bind(area, std::placeholders::_1));
		auto min_area = *std::min_element(areas.begin(), areas.end());
		return min_area;
	}
	if (str == "VERTEXES" && polygons.size() > 0)
	{
		std::vector<int> sizes(polygons.size());
		std::transform(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size();
			});
		auto min_size = *std::min_element(sizes.begin(), sizes.end());
		return min_size;
	}
	return 0;
}

int COUNT(std::string str, const std::vector<Polygon>& polygons)
{
	std::vector<Polygon> sizes;
	if (str == "EVEN")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 == 0; });
	}
	if (str == "ODD")
	{
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(sizes), [](const Polygon& polygon) {
			return polygon.points.size() % 2 != 0; });
	}
	if (std::all_of(str.begin(), str.end(), ::isdigit))
	{
		int count = std::stoi(str);
		std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(sizes), [count](const Polygon& polygon) {
			return polygon.points.size() == static_cast<std::vector<Point>::size_type>(count); });
	}
	int size = sizes.size();
	return size;
}

bool isPointInsidePolygon(const Point& p, const Polygon& polygon) {
	int count = 0;
	for (size_t i = 0; i < polygon.points.size(); i++) {
		const Point& p1 = polygon.points[i];
		const Point& p2 = polygon.points[(i + 1) % polygon.points.size()];
		if (((p1.y > p.y) != (p2.y > p.y)) &&
			(p.x < (p2.x - p1.x) * (p.y - p1.y) / (p2.y - p1.y) + p1.x)) {
			count++;
		}
	}
	return (count % 2 == 1);
}


bool in_frame(const std::vector<Polygon>& polygons, const Polygon& target_poly)
{
	int min_x = std::numeric_limits<int>::max();
	int max_x = std::numeric_limits<int>::min();
	int min_y = std::numeric_limits<int>::max();
	int max_y = std::numeric_limits<int>::min();

	for (const auto& poly : polygons)
	{
		for (const auto& point : poly.points)
		{
			min_x = std::min(min_x, point.x);
			max_x = std::max(max_x, point.x);
			min_y = std::min(min_y, point.y);
			max_y = std::max(max_y, point.y);
		}
	}

	for (const auto& point : target_poly.points)
	{
		if (point.x < min_x || point.x > max_x || point.y < min_y || point.y > max_y)
		{
			return false;
		}
	}

	return true;
}

bool operator==(const Point& lhs, const Point& rhs)
{
	return lhs.x == rhs.x && lhs.y == rhs.y;
}


bool check_permutation(const Polygon& poly1, const Polygon& poly2)
{
	if (poly1.points.size() != poly2.points.size()) {
		return false;
	}

	std::vector<bool> matched(poly2.points.size(), false);
	for (const auto& p1 : poly1.points) {
		bool found = false;
		for (size_t j = 0; j < poly2.points.size(); ++j) {
			if (!matched[j] && p1 == poly2.points[j]) {
				matched[j] = true;
				found = true;
				break;
			}
		}
		if (!found) {
			return false;
		}
	}

	return true;
}

int perms(const std::vector<Polygon>& polygons, const Polygon& target_poly)
{
	int count = 0;
	for (const auto& poly : polygons)
	{
		if (check_permutation(poly, target_poly))
		{
			count++;
		}
	}
	return count;
}




#endif



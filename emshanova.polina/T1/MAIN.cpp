﻿#include <iostream>
#include <sstream>
#include <string>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>
#include "Data.h"

int main()
{
    using nspace::Data;
    using nspace::CompareData;

    std::vector< Data > data;
    std::istringstream iss("    (:key1 1.0d:ke82 \"incor1\":)\n\
                                (:key1 1.1d:key2 0xFFF1:key3 \"string_1\":) aa (:key1 2.1d:key2 0x2FF1:key3 \"string_2\":)\n\
                                (:key1 1.2:key2 0xFFF1:key3 \"incor2\":)\n\
                                (:key 1.3d:key2 0xFFF3:key3 \"incor3\":)\n\
                                (key1 1.3d:key2 0xFFF3:key3 \"incor4\":)\n\
                                (:key1 1.3d:key2 0xFFF3:key3 \"incor5\":\n\
                                (:key1 1.3d:key2 0x-FFF3:key3 \"incor6:)\n\
                                (:key2 0xFFF3:key1 1.3d:key3 \"string_3 is longer\":)\n\
                                (:key1 1.4d:key2 wrong)\n\
                                (:key3 \"string_4\":key1 1.5d:key2 0xFFF5:) (:)\n\
                                (key1 13d:key2 0xFFF3:key3 \"incor5:) (:key2 0xFFF3:key1 1.3d:key3 \"string_5\":)c\n\
                                (:key1 1.5d:key2 0xFFF5:key2 0xFFF5:)\n\
                                (:key2 0x53a:key3 \"string_6\":key1 -1.6d:)\n\
                                (:key0 incorrect)");

    while (!iss.eof())
    {
        if (!iss)
        {
            iss.clear();
        }
        std::copy(
            std::istream_iterator<Data>(iss),
            std::istream_iterator<Data>(),
            std::back_inserter(data)
        );
    }

    std::sort(
        std::begin(data),
        std::end(data),
        CompareData()
    );

    std::cout << "Data:\n";
    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Data >(std::cout, "\n")
    );

    return 0;
}

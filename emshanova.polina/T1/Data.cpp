#include"Data.h"
#include <iomanip>

namespace nspace
{
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && (c != dest.exp))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, DoubleIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return in >> dest.ref >> DelimiterIO{ 'd' };
    }

    std::istream& operator>>(std::istream& in, StringIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return std::getline(in >> DelimiterIO{ '"' }, dest.ref, '"');
    }

    std::istream& operator>>(std::istream& in, LabelIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::string label = "";
        in >> label;
        if (in && label != "key1" && label != "key2" && label != "key3")
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.exp = label;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, ULLIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        return in >> DelimiterIO{ '0' } >> DelimiterIO{ 'x' } >> std::hex >> dest.ref >> std::dec;
    }

    std::istream& operator>>(std::istream& in, Data& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        Data input;

        using sep = DelimiterIO;
        using label = LabelIO;
        using dbl = DoubleIO;
        using str = StringIO;
        using ull = ULLIO;


        bool isFirst = false;
        bool isSecond = false;
        bool isThird = false;

        std::string keyNum;

        in >> sep{ '(' } >> sep{ ':' };
        for (int i = 0; i < 3; ++i)
        {
            in >> label{ keyNum };
            if (keyNum == "key1")
            {
                in >> dbl{ input.key1 };
                isFirst = true;
            }
            else if (keyNum == "key2")
            {
                in >> ull{ input.key2 };
                isSecond = true;
            }
            else if (keyNum == "key3")
            {
                in >> str{ input.key3 };
                isThird = true;
            }
            in >> sep{ ':' };
        } 
        in >> sep{ ')' };
        if (in && isFirst && isSecond && isThird)
        {
            dest = input;
        }
        else
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::ostream& operator<<(std::ostream& out, const Data& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        out << "(:";
        out << "key1 " << std::fixed << std::setprecision(1) << src.key1 << "d:";
        out << "key2 " <<  "0x"<< std::hex << std::uppercase << src.key2 << std::dec << ':';
        out << "key3 " << '"' << src.key3 << '"';
        out << ":)";
        return out;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }


    bool CompareData::operator()(const Data& left, const Data& right) const
    {
        if (left.key1 == right.key1 && left.key2 == right.key2)
        {
            return left.key3.size() < right.key3.size();
        }
        if (left.key1 == right.key1)
        {
            return left.key2 < right.key2;
        }
        return left.key1 < right.key1;
    }
}
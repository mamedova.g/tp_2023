#ifndef _DATA_STRUCT
#define _DATA_STRUCT

#include <complex>
#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <iterator>
#include <vector>
#include <iomanip>
#include <algorithm>
#include <array>

namespace nspace
{

    struct DataStruct
    {
        double key1;
        unsigned long long key2;
        std::string key3;
    };

    struct DelimiterIO
    {
        char exp;
    };

    struct UnsignedLongLongIO
    {
        unsigned long long& ref;
    };

    struct DoubleScienceIO
    {
        double& ref;
    };

    struct StringIO
    {
        std::string& ref;
    };

    struct LabelStringIO
    {
        std::string& exp;
    };

    struct LabelIO
    {
        std::string exp;
    };

    // scope guard ��� �������� ��������� ������ � �������������� ���������
    class iofmtguard
    {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, UnsignedLongLongIO&& dest);
    std::istream& operator>>(std::istream& in, DoubleScienceIO&& dest);
    std::istream& operator>>(std::istream& in, StringIO&& dest);
    std::istream& operator>>(std::istream& in, LabelStringIO&& dest);
    std::istream& operator>>(std::istream& in, LabelIO&& dest);
    std::istream& operator>>(std::istream& in, DataStruct& dest);
    std::ostream& operator<<(std::ostream& out, const DataStruct& dest);

    bool sort(DataStruct a, DataStruct b);
}
#endif

#pragma once
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <algorithm>

// Implementation of ifstream wrapper
class Myifstream {
public:
    Myifstream(const char* input);
    ~Myifstream();
    bool is_open();
    bool eof();
    void close();

    friend Myifstream& operator>>(Myifstream& ifs, std::string& str) {
        ifs.fileStream >> str;
        return ifs;
    }

    operator bool() const;

private:
    std::ifstream fileStream;
};

// Implementation of unordered_map wrapper
template<typename Key, typename Value>
class unordered_mapWrapper {
public:
    Value& operator[](const Key& key) {
        return map[key];
    }

    typename std::unordered_map<Key, Value>::iterator begin() {
        return map.begin();
    }

    typename std::unordered_map<Key, Value>::iterator end() {
        return map.end();
    }

private:
    std::unordered_map<Key, Value> map;
};

// Implementation of vector wrapper
template<typename T>
class vectorWrapper {
public:
    void push_back(const T& value) {
        vec.push_back(value);
    }

    typename std::vector<T>::iterator begin() {
        return vec.begin();
    }

    typename std::vector<T>::iterator end() {
        return vec.end();
    }

    typename std::vector<T>::const_iterator begin() const {
        return vec.begin();
    }

    typename std::vector<T>::const_iterator end() const {
        return vec.end();
    }

private:
    std::vector<T> vec;
};

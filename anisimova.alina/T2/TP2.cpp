#include <vector>
#include <iostream>
#include <algorithm>
#include <functional>
#include <iterator>
#include <numeric>
#include <iterator>
#include <string>
#include <iomanip>
#include <sstream>

namespace nspace {
    struct Point
    {
        int x, y;
    };

    struct Polygon
    {
        std::vector<Point> points;
    };

    struct DelimiterIO
    {
        char exp;
    };

    struct PointIO
    {
        Point& ref;
    };

    class iofmtguard
    {
    public:
        iofmtguard(std::basic_ios< char >& s);
        ~iofmtguard();
    private:
        std::basic_ios< char >& s_;
        char fill_;
        std::streamsize precision_;
        std::basic_ios< char >::fmtflags fmt_;
    };

    std::istream& operator>>(std::istream& in, DelimiterIO&& dest);
    std::istream& operator>>(std::istream& in, PointIO&& dest);
    std::istream& operator>>(std::istream& in, Polygon& dest);
    std::ostream& operator<<(std::ostream& out, const Polygon& dest);
    bool operator==(const Polygon& left, const Polygon& right);
    bool isEqual(const Point& p1, const Point& p2);
    double areaEven(const std::vector< Polygon >& polygons);
    double areaOdd(const std::vector< Polygon >& polygons);
    double areaMean(const std::vector< Polygon >& polygons);
    double areaVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes);
    double maxArea(const std::vector< Polygon >& polygons);
    size_t maxVertexes(const std::vector< Polygon >& polygons);
    double minArea(const std::vector< Polygon >& polygons);
    size_t minVertexes(const std::vector< Polygon >& polygons);
    size_t countEven(const std::vector< Polygon >& polygons);
    size_t countOdd(const std::vector< Polygon >& polygons);
    size_t countNumOfVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes);
    void getResults(std::vector< Polygon >& polygons, std::istream& in);
    size_t sameCount(const std::vector<Polygon>& polygons, const Polygon& target);
}

int main()
{
    using namespace nspace;
    std::istringstream iss("3 (1;1) (4;2) (3;4)  \n\
								4 (0;0) (1;0) (1;1) (0;1)\n\
                                3 (0; 0) (1; 1) (0; 1)\n\
                                4 (1; 1) (2; 1) (2; 2) (1; 2)\n\
                                3 (5; 4) (7; 4) (7; 5)\n\
                                4 (-2; -2) (-1; -2) (-1; -1) (-2; -1) \n\
                                3 (-3; 1) (0; 2) (-1; 4)\n\
                                3 (1; 2) (1; 5) (-1; 3) ");
    std::string lineString;
    std::stringstream lineStream;
    std::vector< Polygon > data;

    while (!iss.eof())
    {
        std::getline(iss, lineString);
        lineStream.clear();
        lineStream << lineString;
        Polygon polygon;
        lineStream >> polygon;
        if (lineStream)
        {
            data.push_back(polygon);
        }
        else
        {
            lineStream.clear();
            lineStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }

    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Polygon >(std::cout, "\n")
    );
    std::istringstream commands("  COUNT ODD  \n\
								COUNT EVEN  \n\
								COUNT 4 \n\
								AREA ODD\n\
								AREA EVEN\n\
								AREA MEAN\n\
								AREA 4\n\
								AREA 7\n\
                                MAX VERTEXES\n\
                                MIN VERTEXES\n\
                                MAX AREA\n\
                                MIN AREA\n\
                                SAME 4 (0;0) (1;0) (1;1) (0;1)\n\
                                LESSAREA 3 (0;0) (2;2) (2;0)");
    iofmtguard guard(std::cout);
    std::cout << std::fixed << std::setprecision(1);
    while (!commands.eof())
    {
        try {
            getResults(data, commands);
        }
        catch (const std::invalid_argument& e) {
            std::cerr << e.what() << '\n';
            lineStream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }

    std::copy(
        std::begin(data),
        std::end(data),
        std::ostream_iterator< Polygon >(std::cout, "\n")
    );
    return 0;
}

namespace nspace
{
	std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
	{
		std::istream::sentry sentry(in);
		if (!sentry)
		{
			return in;
		}
		char c = '0';
		in >> c;
		if (in && (c != dest.exp))
		{
			in.setstate(std::ios::failbit);
		}
		return in;
	}

	std::istream& operator>>(std::istream& in, PointIO&& dest)
	{
		std::istream::sentry sentry(in);
		if (!sentry)
		{
			return in;
		}
		return in >> DelimiterIO{ '(' } >> dest.ref.x >> DelimiterIO{ ';' } >> dest.ref.y >> DelimiterIO{ ')' };
	}

	std::istream& operator>>(std::istream& in, Polygon& dest)
	{
		std::istream::sentry sentry(in);
		if (!sentry)
		{
			return in;
		}
		Polygon input;
		size_t numberOfPoints = 0;
		size_t count = 0;
		in >> numberOfPoints;
		if (numberOfPoints < 1)
		{
			in.setstate(std::ios::failbit);
		}
		for (size_t i = 0; i < numberOfPoints && in; i++)
		{
			Point point;
			in >> PointIO{ point };
			if (in)
			{
				count++;
				input.points.push_back(point);
			}
		}
		if (in && count == numberOfPoints)
		{
			dest = input;
		}
		return in;
	}

	std::ostream& operator<<(std::ostream& out, const Polygon& src)
	{
		std::ostream::sentry sentry(out);
		if (!sentry)
		{
			return out;
		}
		iofmtguard fmtguard(out);
		out << src.points.size() << " ";
		for (size_t i = 0; i < src.points.size(); i++)
		{
			out << "(" << src.points[i].x << ";" << src.points[i].y << ") ";
		}
		return out;
	}
	bool operator==(const Polygon& f1, const Polygon& f2)
	{
		if (f1.points.size() == f2.points.size())
		{
			auto iterator = std::mismatch(f1.points.begin(), f1.points.end(),
				f2.points.begin(), f2.points.end(), isEqual);
			return (iterator.first == f1.points.end());
		}
		else
		{
			return false;
		}
	}
	iofmtguard::iofmtguard(std::basic_ios< char >& s) :
		s_(s),
		fill_(s.fill()),
		precision_(s.precision()),
		fmt_(s.flags())
	{}

	iofmtguard::~iofmtguard()
	{
		s_.fill(fill_);
		s_.precision(precision_);
		s_.flags(fmt_);
	}

	bool isEqual(const Point& p1, const Point& p2)
	{
		return p1.x == p2.x && p1.y == p2.y;
	}
    double findArea(const Polygon& polygon)
    {
        double area = 0.0;
        size_t n = polygon.points.size();

        for (size_t i = 0; i < n; ++i) {
            const Point& currentVertex = polygon.points[i];
            const Point& nextVertex = polygon.points[(i + 1) % n];

            area += (currentVertex.x * nextVertex.y) - (nextVertex.x * currentVertex.y);
        }


        return std::abs(static_cast<double>(area) / 2.0);
    }

    double areaEven(const std::vector< Polygon >& polygons)
    {
        std::vector<Polygon> figures;
        std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
            { return polygon.points.size() % 2 == 0; });
        std::vector<double> areas(figures.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), findArea);
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double areaOdd(const std::vector< Polygon >& polygons)
    {
        std::vector<Polygon> figures;
        std::copy_if(polygons.begin(), polygons.end(), std::back_inserter(figures), [](const Polygon& polygon)
            { return polygon.points.size() % 2 != 0; });
        std::vector<double> areas(polygons.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), findArea);
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double areaMean(const std::vector< Polygon >& polygons)
    {
        std::vector<double> areas(polygons.size());
        std::transform(polygons.begin(), polygons.end(), areas.begin(), findArea);
        double resultArea = std::accumulate(areas.begin(), areas.end(), 0.0);
        double averageArea = resultArea / polygons.size();
        return averageArea;
    }

    double areaVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes)
    {
        std::vector<Polygon> figures;
        std::copy_if(
            polygons.begin(),
            polygons.end(),
            std::back_inserter(figures),
            [numOfVertexes](const Polygon& polygon) {return polygon.points.size() == numOfVertexes; });
        std::vector<double> areas(figures.size());
        std::transform(figures.begin(), figures.end(), areas.begin(), findArea);
        double result = std::accumulate(areas.begin(), areas.end(), 0.0);
        return result;
    }

    double maxArea(const std::vector< Polygon >& polygons)
    {
        auto it = std::max_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return findArea(p1) < findArea(p2);
            });
        return findArea(*it);
    }

    size_t maxVertexes(const std::vector< Polygon >& polygons)
    {
        auto it = std::max_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return p1.points.size() < p2.points.size();
            });
        return it->points.size();
    }

    double minArea(const std::vector< Polygon >& polygons)
    {
        auto it = std::min_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return findArea(p1) < findArea(p2);
            });
        return findArea(*it);
    }

    size_t minVertexes(const std::vector< Polygon >& polygons)
    {
        auto it = std::min_element(polygons.begin(), polygons.end(),
            [](const Polygon& p1, const Polygon& p2) {
                return p1.points.size() < p2.points.size();
            });
        return it->points.size();
    }


    size_t countEven(const std::vector< Polygon >& polygons)
    {
        return std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon)
            { return polygon.points.size() % 2 == 0; });
    }


    size_t countOdd(const std::vector< Polygon >& polygons)
    {
        return std::count_if(polygons.begin(), polygons.end(), [](const Polygon& polygon)
            { return polygon.points.size() % 2 == 1; });
    }

    size_t countNumOfVertexes(const std::vector< Polygon >& polygons, size_t numOfVertexes)
    {
        return std::count_if(polygons.begin(), polygons.end(), [numOfVertexes](const Polygon& polygon)
            { return polygon.points.size() == numOfVertexes; });
    }

    size_t sameCount(const std::vector<Polygon>& polygons, const Polygon& target)
    {
        size_t count = 0;
        for (const Polygon& polygon : polygons)
        {
            if (polygon.points.size() != target.points.size())
                continue;

            bool isSame = true;
            for (size_t i = 0; i < polygon.points.size()-1; ++i)
            {
                if (polygon.points[i].x - polygon.points[i+1].x != target.points[i].x - target.points[i + 1].x || polygon.points[i].y - polygon.points[i+1].y != target.points[i].y - target.points[i + 1].y)
                {
                    isSame = false;
                    break;
                }
            }

            if (isSame)
                count++;
        }
        return count;
    }
    void getResults(std::vector< Polygon >& polygons, std::istream& in)
    {
        std::string INVALID_COMMAND = "INVALID COMMAND";
        std::string command;
        in >> command;
        if (command == "AREA")
        {
            std::string arg;
            in >> arg;
            if (arg == "EVEN")
            {
                std::cout << areaEven(polygons) << std::endl;
            }
            else if (arg == "ODD")
            {
                std::cout << areaOdd(polygons) << std::endl;
            }
            else if (arg == "MEAN")
            {
                if (polygons.size() == 0)
                {
                    throw std::invalid_argument(INVALID_COMMAND);
                }
                std::cout << areaMean(polygons) << std::endl;
            }
            else
            {
                size_t vertexes = 0;
                try
                {
                    vertexes = std::stoull(arg);
                }
                catch (...)
                {
                    throw std::invalid_argument(INVALID_COMMAND);
                }
                std::cout << areaVertexes(polygons, vertexes) << std::endl;
            }
        }
        else if (command == "MAX")
        {
            if (polygons.size() == 0)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            std::string arg;
            in >> arg;
            if (arg == "AREA")
            {
                std::cout << maxArea(polygons) << std::endl;
            }
            else if (arg == "VERTEXES")
            {
                std::cout << maxVertexes(polygons) << std::endl;
            }
            else
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
        }
        else if (command == "MIN")
        {
            if (polygons.size() == 0)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            std::string arg;
            in >> arg;
            if (arg == "AREA")
            {
                std::cout << minArea(polygons) << std::endl;
            }
            else if (arg == "VERTEXES")
            {
                std::cout << minVertexes(polygons) << std::endl;
            }
            else
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
        }
        else if (command == "COUNT")
        {
            std::string arg;
            in >> arg;
            if (arg == "EVEN")
            {
                std::cout << countEven(polygons) << std::endl;
            }
            else if (arg == "ODD")
            {
                std::cout << countOdd(polygons) << std::endl;
            }
            else
            {
                size_t vertexes = 0;
                try
                {
                    vertexes = std::stoull(arg);
                }
                catch (...)
                {
                    throw std::invalid_argument(INVALID_COMMAND);
                }
                std::cout << countNumOfVertexes(polygons, vertexes) << std::endl;
            }
        }
        else if (command == "SAME")
        {
            Polygon targetPolygon;
            in >> targetPolygon;
            if (!in)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }

            size_t count = sameCount(polygons, targetPolygon);
            std::cout << count << std::endl;
        }
        else if (command == "LESSAREA")
        {
            Polygon comparePolygon;
            in >> comparePolygon;
            if (!in)
            {
                throw std::invalid_argument(INVALID_COMMAND);
            }
            double compareArea = findArea(comparePolygon);
            int count = 0;
            for (const Polygon& polygon : polygons)
            {
                double area = findArea(polygon);
                if (area < compareArea)
                {
                    count++;
                }
            }
            std::cout << count << std::endl;
        }
        else
        {
            in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
}
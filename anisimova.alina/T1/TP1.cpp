
#include "TP1.h"

namespace nspace {
    std::istream& operator>>(std::istream& in, DelimiterIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && (c != dest.exp))
        {
            in.setstate(std::ios::failbit);
        }
        return in;
    }
    std::istream& operator>>(std::istream& in, DoubleIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char delim = '0';
        double num;
        in >> num >> std::noskipws >> delim >> std::skipws;
        if (!(delim == 'd' || delim == 'D'))
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = num;
        }
        return in;
    }
    std::istream& operator>>(std::istream& in, CharIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char delim1 = '0';
        char delim2 = '0';
        char lit = '0';
        in >> delim1 >> std::noskipws >> lit >> std::noskipws >> delim2 >> std::skipws;
        if (in && ( delim1 != '`' || delim2 != '`' ) )
        {
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = lit;
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, StringIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        char c = '0';
        in >> c;
        if (in && c != '"')
        {
            in.setstate(std::ios::failbit);
        }

        std::string data;
        in >> std::noskipws >> c;
        while (in)
        {
            if (c == '\n' || c == '"')
            {
                break;
            }
            data += c;
            in >> c;
        }
        in >> std::skipws;

        if (in && c != '"')
        {
            dest.ref = "";
            in.setstate(std::ios::failbit);
        }
        else
        {
            dest.ref = data;
        }

        return in;
    }

    std::istream& operator>>(std::istream& in, LabelIO&& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        std::string data;
        in >> data;
        std::string keyNum;
        bool isCorrect = false;
        for (int i = 1; i <= 3 && (!isCorrect); ++i)
        {
            keyNum = std::to_string(i);
            if (data == "key" + keyNum)
            {
                dest.exp = keyNum;
                isCorrect = true;
            }
        }
        if (!isCorrect)
        {
            dest.exp = "";
            in.setstate(std::ios::failbit);
        }
        return in;
    }

    std::istream& operator>>(std::istream& in, Data& dest)
    {
        std::istream::sentry sentry(in);
        if (!sentry)
        {
            return in;
        }
        iofmtguard formatGuard(in);
        {
            using sep = DelimiterIO;
            using label = LabelIO;
            using dbl = DoubleIO;
            using str = StringIO;
            using chr = CharIO;

            bool isFirst = false;
            bool isSecond = false;
            bool isThird = false;

            Data input;
            in >> std::skipws >> sep{ '(' } >> std::noskipws >> sep{ ':' } >> std::skipws;
            for (int i = 0; i < 3; ++i)
            {
                std::string numKey;
                in >> label{ numKey };
                if (numKey == "1")
                {
                    if (isFirst)
                    {
                        in.setstate(std::ios::failbit);
                        break;
                    }
                    in >> dbl{ input.key1 };
                    isFirst = true;
                }
                else if (numKey == "2")
                {
                    if (isSecond)
                    {
                        in.setstate(std::ios::failbit);
                        break;
                    }
                    in >> chr{ input.key2 };
                    isSecond = true;
                }
                else if (numKey == "3")
                {
                    if (isThird)
                    {
                        in.setstate(std::ios::failbit);
                        break;
                    }
                    in >> str{ input.key3 };
                    isThird = true;
                }
                in >> sep{ ':' };
            }

            in >> sep{ ')' };
            if (in)
            {
                dest = input;
            }
            return in;
        }
    }

    std::ostream& operator<<(std::ostream& out, const Data& src)
    {
        std::ostream::sentry sentry(out);
        if (!sentry)
        {
            return out;
        }
        iofmtguard fmtguard(out);
        out << "(: ";
        out << "key1 " << std::fixed << std::setprecision(1) << src.key1 << "d:";
        out << "key2 " << "`" << src.key2 << "`:";
        out << "key3 " << '\"' << src.key3 << '\"';
        out << " :)";
        return out;
    }
    bool CompareDataStruct::operator()(const Data& first, const Data& second) const
    {
        if (first.key1 == second.key1 && first.key2 == second.key2)
        {
            return first.key3.length() < second.key3.length();
        }
        if (first.key1 == second.key1)
        {
            return first.key2 < second.key2;
        }
        return first.key1 < second.key1;
    }

    iofmtguard::iofmtguard(std::basic_ios< char >& s) :
        s_(s),
        fill_(s.fill()),
        precision_(s.precision()),
        fmt_(s.flags())
    {}

    iofmtguard::~iofmtguard()
    {
        s_.fill(fill_);
        s_.precision(precision_);
        s_.flags(fmt_);
    }
}